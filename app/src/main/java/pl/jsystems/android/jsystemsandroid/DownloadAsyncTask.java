package pl.jsystems.android.jsystemsandroid;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.ProgressBar;

public class DownloadAsyncTask extends AsyncTask<Void, Integer, Void> {
    private ProgressBar progressBar;
    private Button button;

    public DownloadAsyncTask(ProgressBar progressBar, Button button) {
        this.progressBar = progressBar;
        this.button = button;
    }

    @Override
    protected void onPreExecute() {
        progressBar.setProgress(0);
        button.setEnabled(false);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        button.setEnabled(true);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setProgress(values[0]);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        for (int i = 1; i <= 100; i++) {
            publishProgress(i);
            Log.d("AsyncTask", ""+i);
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
