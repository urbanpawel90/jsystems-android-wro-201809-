package pl.jsystems.android.jsystemsandroid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class LifecycleLogActivity extends AppCompatActivity {
    private String getActivityName() {
        return this.getClass().getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(getActivityName(), "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(getActivityName(), "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(getActivityName(), "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(getActivityName(), "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(getActivityName(), "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(getActivityName(), "onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(getActivityName(), "onRestart");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(getActivityName(), "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(getActivityName(), "onRestoreInstanceState");
    }
}
