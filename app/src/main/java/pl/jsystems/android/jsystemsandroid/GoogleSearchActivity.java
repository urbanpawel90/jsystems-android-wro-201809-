package pl.jsystems.android.jsystemsandroid;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class GoogleSearchActivity extends LifecycleLogActivity implements View.OnClickListener {
    public static final int CALL_PERMISSIONS_REQUEST_CODE = 112;

    private TextView tvPhrase;
    private Button btnGoogle;
    private ProgressBar pbProgress;
    private Button btnStart;

    private String phrase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_search);

        tvPhrase = findViewById(R.id.tv_phrase);
        btnGoogle = findViewById(R.id.btn_google);
        pbProgress = findViewById(R.id.pb_progress);
        btnStart = findViewById(R.id.btn_start);

        btnGoogle.setOnClickListener(this);

        if (getIntent().hasExtra("phrase")) {
            phrase = getIntent().getStringExtra("phrase");
        }

        tvPhrase.setText(phrase);
    }

    @Override
    public void onClick(View v) {
        // https://www.google.com/search?q=
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.google.com/search?q=" + phrase));
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mi_call) {
            onCallItemClick();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void onCallItemClick() {
        if (!phrase.matches("^(\\+|00)?[0-9]+$")) {
            Toast.makeText(this, "To nie jest numer telefonu", Toast.LENGTH_LONG).show();
            return;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            performCall();
        } else {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void performCall() {
        Intent callIntent = new Intent(Intent.ACTION_CALL,
                Uri.parse("tel:" + phrase));
        try {
            startActivity(callIntent);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(this, "Twoje urządzenie nie potrafi dzwonić!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CALL_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                performCall();
            } else {
                Toast.makeText(this, "Bardzo mi przykro!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void startTask(View v) {
        new DownloadAsyncTask(pbProgress, btnStart).execute();
    }

}
